#add DNS server
echo "nameserver 192.168.1.21" >> /etc/resolv.conf
# setup
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules

# mongodb
# apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
# echo "deb http://repo.mongodb.org/apt/debian "$(lsb_release -sc)"/mongodb-org/3.0 main" | tee /etc/apt/sources.list.d/mongodb-org-3.0.list

apt-get update
# remove unneeded/deprecated packages
apt-get autoremove

# base components
apt-get install -y git
# apt-get install -y mongodb-org

# development components
npm install -g bower yo grunt-cli less

# MEAN stack
npm install -g generator-angular-fullstack

# Meteor
# curl https://install.meteor.com/ | sh

apt-get install -y ruby-full
gem install sass