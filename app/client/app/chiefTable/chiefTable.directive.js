'use strict';

angular.module('appChief')
	.directive('chiefTable', function () {
		return {
			templateUrl: 'app/chiefTable/chiefTable.html',
			restrict: 'E',
			replace: true,
			scope: {
				'records': '=',
				'headers': '=',
				'selectRecord': '&',
				'selectedRecord': '=',
				'addRecord': '&',
				'buttonText': '=',
				'columnCount': '='
			},

			link: function($scope){
				$scope.checked = [];
				$scope.reverse = false;
				$scope.currentOrder = 'id';
				$scope.columnWidth = (100/$scope.columnCount)+'%';
				console.log('the width = '+$scope.columnWidth);

				$scope.orderBy = function(attr){
					$scope.currentOrder = attr;
					switch($scope.reverse){
						case true:
							$scope.reverse = false;
							break;
						case false:
							$scope.reverse = true;
							break;
					}
				};

				$scope.onlyCurrent = function(property) {
					var ret = false;
					if($scope.currentId === property.id){
						ret = true;
					}
					return ret;
				};

				$scope.toggleChecked = function(record) {
					if($scope.isInArray(record.id, $scope.checked) === false){
						$scope.checked.push(record.id);
						console.log('Adding '+record.id);
					}
					else{
						$scope.checked.splice($scope.checked.indexOf(record.id), 1);
						console.log('Removing '+record.id);
					}
					$scope.selectedRecord = $scope.checked;
					console.log($scope.selectedRecord);
				};

				$scope.clearChecked = function(){
					$scope.checked = [];
				};

				$scope.isInArray = function(value, array) {
				  return array.indexOf(value) > -1;
				};

				$scope.scaleChiefHeight = function(size){
					if(size === 'small'){
						$scope.chiefHeight = '300px';
					}
					else{
						$scope.chiefHeight = $(window).height() - 270+'px';
					}
				};

				$scope.$watch('selectedRecord', function() {
					if($scope.selectedRecord){
						$scope.scaleChiefHeight('small');
					}else if( $scope.selectedRecord === []){
						$scope.scaleChiefHeight('small');
					}else{
						$scope.scaleChiefHeight('big');
					}
				});

				$scope.chiefHeight = $(window).height() - 270+'px';
				$(window).resize(function() {
				  $scope.chiefHeight = $(window).height() - 270 +'px';
				});
			}
		};
	})

	.directive('transformDate', function() {
		return {
			restrict: 'A',
			require: 'ngModel',
			
			link: function(scope, element, attrs, ngModel) {
				if(ngModel) {
					ngModel.$parsers.push(function (value) {

						var result = new moment(value).format('x');
						console.log('value: '+value);
						console.log(result);
						return result;
					});
					ngModel.$formatters.push(function (value) {
						if(value){
							return new Date(value);
						}
						return null;
					});
				}
			}
		};
	});