'use strict';
(function() {

angular.module('appUsers')
  .controller('UsersCtrl', function ($scope, $log, $http, $anchorScroll, $location, $timeout, DMSSERVER, UserFactory, RoleFactory, SystemFactory) {
	var userFromServer = {};
	$scope.selectedMenuItem = 'Users';
	$scope.buttonText = 'adduser';
	$scope.selectedUser = null;
	$scope.pass = {};
	$scope.showDetails = false;
	$scope.showAddNewUser = false;
	$scope.currentOrder = 'id';
	$scope.newuser = {};
	$scope.headers = [
		{
			'key': 'id',
			'displayName': 'id'
		},
		{
			'key': 'name',
			'displayName': 'username'
		},
		{
			'key': 'email',
			'displayName': 'email'
		},
		{
			'key': 'fullName',
			'displayName': 'fullname'
		}
	];

	UserFactory.get().then(function(users){
		$scope.users = users;
	});

	RoleFactory.get().then(function(roles){
		$scope.roles = roles;
	});

	$scope.setSelectedUser = function(selectedRecord) {
		angular.copy(selectedRecord, userFromServer);
		
		if (!$scope.selectedUser) {
			var index = ($scope.users.indexOf(selectedRecord)-2);
			if (index > 0) {
				$location.hash($scope.users[index].id);
			}else {
				$location.hash(selectedRecord.id);
			}
		}
		$scope.selectedUser = selectedRecord;
	};

	$scope.updateUser = function(){
		if(angular.equals(userFromServer, $scope.selectedUser)){
			console.log('the same, not saving');
		}else{
			console.log('not the same, saving');
			UserFactory.save($scope.selectedUser);
			//UserFactory.save($scope.selectedUser).then(function(){
				//$scope.selectedUser.activationDate = new Date($scope.selectedUser.activationDate);
				//$scope.selectedUser.deactivationDate = new Date($scope.selectedUser.deactivationDate);
			//});
		}
	};

	$scope.deleteUser = function(){
		UserFactory.remove($scope.selectedUser);
		$scope.selectedUser = null;
	};

	$scope.openNewUser = function(){
		var user = $scope.users.create({});
		$scope.selectedUser = user;
		$scope.addNewUserView = true;
	};

	$scope.saveNewUser = function(){
		UserFactory.save($scope.selectedUser);
		$scope.addNewUserView = false;
	};

	$scope.cancelNewUser = function(){
		UserFactory.cancel($scope.selectedUser);
		$scope.selectedUser = null;
		$scope.addNewUserView = false;
	};

	$scope.resetPassword = function(passA, passB){
		var regEx = new RegExp(SystemFactory.getByParameter('PW_PROFILE'));
		var correctpassword = regEx.test(passA);

		if(passA === passB && correctpassword){
			console.log('passwords are the same and correct, setting new password');
			$scope.selectedUser.password = passA;
			UserFactory.save($scope.selectedUser);
		}else if(passA === passB){
			var regExFixMessage = SystemFactory.getByParameter('PW_PROFILE_DESC');
			console.log(regExFixMessage);
		}else{
			console.log('Wachtwoorden zijn niet hetzelfde');
			$scope.pass = {};
		}
	};

	$scope.getUserName = function(role){
		return function(user){
			for(var i =0; i < user.roleIds.length; i++){
				//$log.debug(user.name + ' ' + user.roleIds[i] +' vs '+role.id);
				if(user.roleIds[i] === role.id){
					return true;
				}
			}
			return false;
		};
	};

  });
})();
