'use strict';

angular.module('appUsers', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('users', {
				url: '/users',
				templateUrl: 'app/users/users.html',
				controller: 'UsersCtrl'
			});
	})

	.factory('UserModel', function(Restangular){
		var restUsers = Restangular.all('user');

		Restangular.extendModel('user', function(obj) {
			return angular.extend(obj, {
				hasRole : function(roleId) {
					if (this.roleIds) {
						return this.roleIds.indexOf(roleId) > -1;
					}
					return false;
				},
				toggleRole : function(roleId) {
					if (this.roleIds) {
						var index = this.roleIds.indexOf(roleId);
						
						if (index > -1) {
							this.roleIds.splice(index, 1);
						}else {
							this.roleIds.push(roleId);
						}
						this.save();
					}
				}
			});
        });

		Restangular.extendCollection('user', function(collection){
				collection.create = function(userData){
						var user = Restangular.restangularizeElement(this.parentResource, userData, 'user');
						return user;
				};
				return collection;
		});
		return restUsers;
	})

	.factory('UserFactory', function($q, UserModel){
		var factory = {};
		var users = [];

		factory.get = function(){
			var deferred = $q.defer();
			UserModel.getList().then(function(usersfromserver){
				users = usersfromserver;
				deferred.resolve(users);
			});
			return deferred.promise;
		};

		factory.save = function(user){
			var deferred = $q.defer();
			if(!user.id){
				UserModel.post(user).then(function(newuser){
					users.push(newuser);
					deferred.resolve(newuser);
			 });

			}else{
				user.save().then(function(saveduser){
					var index = users.indexOf(user);
					angular.copy(saveduser, users[index]);
					deferred.resolve(saveduser);
				});

			}
			return deferred.promise;
		};

		factory.remove = function(user){
			user.remove().then(function(){
				var index = users.indexOf(user);
				users.splice(index, 1);
			});
		};

		factory.cancel = function(user){
			if(user.id){
				user.get().then(function(canceluser){
					var index = users.indexOf(user);
					angular.copy(canceluser, users[index]);
				});
			}
			
		};

		return factory;
	})
	
	;
