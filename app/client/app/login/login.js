'use strict';

angular
  .module('appLogin', ['ui.router'])
  .config(function ($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl'
      });
  })
  
  .service('Session', [function() {
	  var _data = null;

	  this.save = function(data) {
	    _data = data;
	  };

	  this.destroy = function() {
	    _data = null;
	  };

	  this.check = function() {
	    return (_data !== null);
	  };

	  this.getAccessToken = function() {
	    return _data.data.token;
	  };

	  this.getAuthorized = function() {
	    return (_data !== null && _data.data.authenticated);
	  };

	this.getName = function() {
		if(_data.data.fullName){
			return _data.data.fullName;
		}
		return _data.data.name;
	  };
	}])

	.factory('Auth', function($rootScope, $location, $http, $q, Session, DMSSERVER) {
		var auth = {};

		auth.login = function (credentials) {
		  var deferred = $q.defer();

		  console.log(credentials);
			$http
				.post(DMSSERVER+'/auth/login', credentials)
				.then(function(response){
					console.log(response);
					Session.save(response);
					$rootScope.$broadcast('userLogin');
					return deferred.resolve();
				}, function(){
					deferred.reject();
				});
		  return deferred.promise;
		};

		auth.logout = function() {
			Session.destroy();
			$rootScope.$broadcast('userLogout');
			$location.path('/');
		};

		auth.isLoggedIn = function() {
			return Session.check();
		};

		auth.canEdit = function() {
			return Session.check() && Session.getAuthorized();
		};
	return auth;
	})

	// angular.module('appLogin')
	.controller('LoginCtrl', function ($scope, $resource, $location, Auth) {

		$scope.username = 'coen';
		$scope.password = 'coen';

		$scope.doLogin = function(){
			Auth.login({username:$scope.username,password:$scope.password}).then(
				function(response) {
					$scope.checking = false;
					$location.url('/main');
				},
				function(error) {
					console.log(error);
					$scope.checking = false;
					$scope.errorMsg = 'Gebruikersnaam of wachtwoord onjuist';
				}
			);
			
		};

	});
