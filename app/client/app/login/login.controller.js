'use strict';

angular.module('appLogin')
	.controller('LoginCtrl', function ($rootScope, $scope, $resource, $location, Auth) {
	$scope.username = '';
	$scope.password = '';

	$scope.doLogin = function(){
		Auth.login({username:$scope.username,password:$scope.password}).then(
			function(response){
				console.log(response);
				$scope.broadcastAuth();
				$location.path('/');
			},
			function(error) {
				$scope.broadcastAuth();
				console.log(error);
				$scope.errorMsg = 'Gebruikersnaam of wachtwoord onjuist';
			}
		);
	};

	$scope.broadcastAuth = function(){
		$rootScope.$broadcast('AuthChanged');
	};
});
