'use strict';

angular
	.module('appChief', [
		'appMenu',
		'appLogin',
		'appUsers',
		'appRoles',
		'appWorklists',
		'appSystem',
		'ngCookies',
		'ngResource',
		'ngSanitize',
		'ui.router',
		'ui.bootstrap',
		'ui.codemirror',
		'validation.match',
		'pascalprecht.translate',
		'restangular'
	])

	.config(function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $translateProvider, RestangularProvider) {
		$translateProvider.useStaticFilesLoader({'prefix': 'assets/translate/lang-', 'suffix':'.json'});
		$translateProvider.useSanitizeValueStrategy('sanitize');
		$translateProvider.preferredLanguage('en');

		RestangularProvider.setBaseUrl('http://192.168.1.14:8080');
		$locationProvider.html5Mode(true);
		$httpProvider.interceptors.push('authInterceptor');
	})

	.run(function($rootScope, $log, $state, $injector, $location, Auth, Session, SystemFactory) {
		//TODO -> remove row below later
		Session.save({'data': {'token': 'ABC', 'fullName':'Coen Dekker', 'name':'coen'}});
		SystemFactory.get();

		$rootScope.$on('$AuthChanged', function(){
			if (Auth.isLoggedIn()) {
				SystemFactory.get();
			}
		});

		$rootScope.$on('$stateChangeStart', function(){
			console.log('stateChangeStart');
			// if (next.authenticate) {
				// Auth.isLoggedIn(function(loggedIn) {
					if (!Auth.isLoggedIn() && $location.path() !== '/login' ) {
						console.log('go to login');
						//event.preventDefault();
						//$state.go('login');
						$location.path('/login');
					}
				// });
			// }
		});
	})

	.value('DMSSERVER', 'http://192.168.1.14:8080')

	.factory('authInterceptor', function($rootScope, $q, $cookies, $injector, Session) {
		var state;
		return {
			// Add authorization token to headers
			request: function(config) {
				if (Session.check()) {
					config.headers['X-TOKEN'] = Session.getAccessToken();
				}

				// config.headers = config.headers || {};
				// if ($cookies.get('token')) {
				// 	config.headers.Authorization = 'Bearer ' + $cookies.get('token');
				// }
				return config;
			},
			// Intercept 401s and redirect you to login
			responseError: function(response) {
				if (response.status === 401) {
					(state || (state = $injector.get('$state'))).go('login');
					// remove any stale tokens
					$cookies.remove('token');
					return $q.reject(response);
				}
				else {
					return $q.reject(response);
				}
			}
		};
	})

	.filter('emptyToEnd', function () {
		return function (array, key) {
			if(!angular.isArray(array)) {
				return;
			}
			var present = [];
			for (var i=0; i < array.length; i++) {
				present.push(array[i]);

				if (present[i][key] === null) {
					present[i][key] = '';
				}
			}
			return present;
		};
	});