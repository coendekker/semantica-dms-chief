'use strict';

angular.module('appRoles', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('roles', {
				url: '/roles',
				templateUrl: 'app/roles/roles.html',
				controller: 'RolesCtrl'
			});
	})

	.factory('RoleModel', function(Restangular){
		var restRoles = Restangular.all('role');

		Restangular.extendModel('role', function(obj) {
			return angular.extend(obj, {
				hasUser : function(userId) {
					if (this.userIds) {
						return this.userIds.indexOf(userId) > -1;
					}
					return false;
				},
				toggleUser : function(userId) {
					if (this.userIds) {
						var index = this.userIds.indexOf(userId);
						
						if (index > -1) {
							this.userIds.splice(index, 1);
						}else {
							this.userIds.push(userId);
						}
						this.save();
					}
				},
				hasWorklist : function(workId) {
					if (this.workIds) {
						return this.workIds.indexOf(workId) > -1;
					}
					return false;
				},
				toggleWorklist : function(workId) {
					if (this.workIds) {
						var index = this.workIds.indexOf(workId);
						
						if (index > -1) {
							this.workIds.splice(index, 1);
						}else {
							this.workIds.push(workId);
						}
						this.save();
					}
				}
			});
        });

		Restangular.extendCollection('role', function(collection){
				collection.create = function(roleData){
						var role = Restangular.restangularizeElement(this.parentResource, roleData, 'role');
						return role;
				};
				return collection;
		});
		return restRoles;
	})

	.factory('RoleFactory', function($q, RoleModel){
		var factory = {};
		var roles = [];

		factory.get = function(){
			var deferred = $q.defer();
			RoleModel.getList().then(function(rolesfromserver){
				roles = rolesfromserver;
				deferred.resolve(roles);
			});
			return deferred.promise;
		};

		factory.save = function(role){
			var deferred = $q.defer();
			if(!role.id){
				RoleModel.post(role).then(function(newrole){
					console.log(newrole);
					roles.push(newrole);
					deferred.resolve(newrole);
			 });

			}else{
				role.save().then(function(savedrole){
					var index = roles.indexOf(role);
					angular.copy(savedrole, roles[index]);
					deferred.resolve(savedrole);
				});

			}
			return deferred.promise;
		};

		factory.remove = function(role){
			role.remove().then(function(){
				var index = roles.indexOf(role);
				roles.splice(index, 1);
			});
		};

		factory.cancel = function(role){
			if(role.id){
				role.get().then(function(cancelrole){
					var index = roles.indexOf(role);
					angular.copy(cancelrole, roles[index]);
				});
			}
			
		};
		return factory;
	});
