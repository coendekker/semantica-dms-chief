'use strict';
(function() {

angular.module('appRoles')
  .controller('RolesCtrl', function ($scope, $log, $http, $resource, DMSSERVER, RoleFactory, UserFactory, WorklistFactory) {
	var roleFromServer = {};
    $scope.selectedMenuItem = 'Roles';
    $scope.buttonText = 'addrole';
    $scope.selectedRole = null;
	$scope.showDetails = false;
	$scope.showAddNewRole = false;
	$scope.currentOrder = 'id';
	$scope.newrole = {};
	$scope.headers = [
		{
			'key': 'id',
			'displayName': 'id',
			'type': 'number'
		},
		{
			'key': 'name',
			'displayName': 'rolename',
			'type': 'string'
		},
		{
			'key': 'creationDate',
			'displayName': 'creationdate',
			'type': 'date'
		},
		{
			'key': 'activationDate',
			'displayName': 'activationdate',
			'type': 'date'
		},
		{
			'key': 'deactivationDate',
			'displayName': 'deactivationdate',
			'type': 'date'
		}
	];

	RoleFactory.get().then(function(roles){
		$scope.roles = roles;
		console.log($scope.roles);
	});

	UserFactory.get().then(function(users){
		$scope.users = users;
	});

	WorklistFactory.get().then(function(worklists){
		$scope.worklists = worklists;
	});

	$scope.setSelectedRole = function(selectedRecord) {
		angular.copy(selectedRecord, roleFromServer);
		$scope.selectedRole = selectedRecord;
	};

	$scope.updateRole = function(){
		if(angular.equals(roleFromServer, $scope.selectedRole)){
			console.log('the same, not saving');
		}else{
			console.log('not the same, saving');
			RoleFactory.save($scope.selectedRole);
		}
	};

	$scope.deleteRole = function(){
		RoleFactory.remove($scope.selectedRole);
		$scope.selectedRole = null;
	};
	
	$scope.openNewRole = function(){
		var role = $scope.roles.create({});
		$scope.selectedRole = role;
		$scope.addNewRoleView = true;
	};

	$scope.saveNewRole = function(){
		RoleFactory.save($scope.selectedRole);
		$scope.addNewRoleView = false;
	};

	$scope.cancelNewRole = function(){
		RoleFactory.cancel($scope.selectedRole);
		$scope.selectedRole = null;
		$scope.addNewRoleView = false;
	};

	var Users = $resource(DMSSERVER+'/user');
	Users.query(function(users) {
		$scope.users = users;
	});

	$scope.getRoleName = function(user){
		return function(role){
			for(var i =0; i < user.roleIds.length; i++){
				if(user.roleIds[i] === role.id){
					return true;
				}
			}
			return false;
		};
	};

	$scope.toDate = function(date){
		return new Date(date);
	};


  });
})();
