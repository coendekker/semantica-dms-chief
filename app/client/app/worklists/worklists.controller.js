'use strict';
(function() {

angular.module('appWorklists')
  .controller('WorklistsCtrl', function ($scope, $log, $http, $resource, $document, $element, DMSSERVER, WorklistFactory, RoleFactory) {
	var worklistFromServer = {};
	$scope.selectedMenuItem = 'Worklist';
	$scope.buttonText = 'addworklist';
	$scope.selectedWorklist = null;
	$scope.showAddNewWorklist = false;
	$scope.currentOrder = 'id';
	$scope.newworklist = {};
	$scope.headers = [
		{
			'key': 'id',
			'displayName': 'ID'
		},
		{
			'key': 'name',
			'displayName': 'worklist'
		},
		{
			'key': 'displayName',
			'displayName': 'displayname'
		},
		{
			'key': 'category',
			'displayName': 'category'
		},
		{
			'key': 'seq',
			'displayName': 'sequence'
		}
	];

	WorklistFactory.get().then(function(worklists){
		$scope.worklists = worklists;
	});

	RoleFactory.get().then(function(roles){
		$scope.roles = roles;
	});

	$scope.setSelectedWorklist = function(selectedRecord) {
		angular.copy(selectedRecord, worklistFromServer);
		$scope.selectedWorklist = selectedRecord;
	};

	$scope.updateWorklist = function(){
		if(angular.equals(worklistFromServer, $scope.selectedWorklist)){
			console.log('the same, not saving');
		}else{
			console.log('not the same, saving');
			WorklistFactory.save($scope.selectedWorklist);
			worklistFromServer = $scope.selectedWorklist;
		}
	};

	$scope.deleteWorklist = function(){
		WorklistFactory.remove($scope.selectedWorklist);
		$scope.selectedWorklist = null;
	};

	$scope.openNewWorklist = function(){
		var worklist = $scope.worklists.create({});
		$scope.selectedWorklist = worklist;
		$scope.addNewWorklistView = true;
	};

	$scope.saveNewWorklist = function(){
		WorklistFactory.save($scope.selectedWorklist);
		$scope.addNewWorklistView = false;
	};

	$scope.cancelNewWorklist = function(){
		WorklistFactory.cancel($scope.selectedWorklist);
		$scope.selectedWorklist = null;
		$scope.addNewWorklistView = false;
	};

	$scope.editorOptions = {
		fullScreen: false,
		value: new Array(8).join("Sample Text to check the row\n"),
		lineNumbers: true,
		lineWrapping : false,
		autoRefresh:true,
		theme: 'lesser-dark',
		styleActiveLine: true,
		fixedGutter:true,
		lint:true, 
		coverGutterNextToScrollbar:false,
		gutters: ['CodeMirror-lint-markers'],
		mode: {name: 'sql', mime: 'text/x-plsql'},
		indentUnit: 4,
		extraKeys: {
        	"F11": function(cm) {
          		cm.setOption("fullScreen", !cm.getOption("fullScreen"));
        	},
        	"Esc": function(cm) {
          		if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
        	}
      	}
    };
    
  });

})();

