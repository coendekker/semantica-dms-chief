'use strict';

angular.module('appWorklists', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('worklists', {
				url: '/worklists',
				templateUrl: 'app/worklists/worklists.html',
				controller: 'WorklistsCtrl'
			});
	})

	.factory('WorklistModel', function(Restangular){
		var restWorklists = Restangular.all('worklist');

		Restangular.extendModel('worklist', function(obj) {
			return angular.extend(obj, {
				hasRole : function(roleId) {
					if (this.roleIds) {
						return this.roleIds.indexOf(roleId) > -1;
					}
					return false;
				},
				toggleRole : function(roleId) {
					if (this.roleIds) {
						var index = this.roleIds.indexOf(roleId);
						
						if (index > -1) {
							this.roleIds.splice(index, 1);
						}else {
							this.roleIds.push(roleId);
						}
						this.save();
					}
				}
			});
		});
		
		Restangular.extendCollection('worklist', function(collection){
				collection.create = function(worklistData){
						var worklist = Restangular.restangularizeElement(this.parentResource, worklistData, 'worklist');
						return worklist;
				};
				return collection;

		});
		return restWorklists;
	})

	.factory('WorklistFactory', function($q, WorklistModel){
		var factory = {};
		var worklists = [];

		factory.get = function(){
			var deferred = $q.defer();
			WorklistModel.getList().then(function(usersfromserver){
				worklists = usersfromserver;
				deferred.resolve(worklists);
			});
			return deferred.promise;
		};

		factory.save = function(worklist){
			var deferred = $q.defer();
			if(!worklist.id){
				WorklistModel.post(worklist).then(function(newuser){
					worklists.push(newuser);
					deferred.resolve(newuser);
				});

			}else{
				worklist.save().then(function(saveduser){
					var index = worklists.indexOf(worklist);
					angular.copy(saveduser, worklists[index]);
					deferred.resolve(saveduser);
				});

			}
			return deferred.promise;
		};

		factory.remove = function(worklist){
			worklist.remove().then(function(){
				var index = worklists.indexOf(worklist);
				worklists.splice(index, 1);
			});
		};

		factory.cancel = function(worklist){
			if(worklist.id){
				worklist.get().then(function(cancelworklist){
					var index = worklists.indexOf(worklist);
					angular.copy(cancelworklist, worklists[index]);
				});
			}
			
		};

		return factory;
	});

