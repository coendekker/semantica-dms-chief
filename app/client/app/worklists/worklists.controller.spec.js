'use strict';

describe('Controller: WorklistsCtrl', function () {

  // load the controller's module
  beforeEach(module('appWorklists'));

  var WorklistsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    WorklistsCtrl = $controller('WorklistsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    1.should.equal(1);
  });
});
