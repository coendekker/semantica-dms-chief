'use strict';

describe('Controller: SystemCtrl', function () {

	// load the controller's module
	beforeEach(module('appSystem'));

	var SystemCtrl, scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function ($controller, $rootScope) {
		scope = $rootScope.$new();
		SystemCtrl = $controller('SystemCtrl', {
			$scope: scope
		});
	}));

	it('should ...', function () {
		1.should.equal(1);
	});
});
