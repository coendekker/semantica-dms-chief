'use strict';
(function() {

angular.module('appSystem')
	.controller('SystemCtrl', function ($scope, SystemFactory) {
		var systemsettingsfromserver = [];
		SystemFactory.get().then(function(response){
			var systemsettingsfromserver = response;
			$scope.systemsettings = response;

			createVarsFormObjectsInArray($scope.systemsettings);
			
			$scope.servletSettings = [
				$scope.EXTERNAL_BASE_URL, 
				$scope.SERVLET_HOST, 
				$scope.SERVLET_PORT, 
				$scope.SERVLET_PROTOCOL, 
				$scope.SESSION_TIMEOUT, 
				$scope.WORKBENCH_CONTEXT
			];

			$scope.emailSettings = [
				$scope.REPLY_TO_ADDRESS, 
				$scope.SMTP_AUTH_PASSWORD, 
				$scope.SMTP_AUTH_USERNAME, 
				$scope.SMTP_HOST, 
				$scope.SMTP_PORT, 
				$scope.SUPPORT_1ST_LINE_EMAIL, 
				$scope.SUPPORT_1ST_LINE_SUBJECT_TOKEN, 
				$scope.SUPPORT_2ND_LINE_EMAIL, 
				$scope.SUPPORT_2ND_LINE_SUBJECT_TOKEN
			];

			$scope.officeSettings = [
				$scope.AUTO_UPDATE_OFFICE, 
				$scope.OPEN_OFFICE_PDF_CACHE, 
				$scope.OPEN_OFFICE_PDF_LOCATION
			];

			$scope.storageSettings = [
				$scope.DEFAULT_SAVESTYLE, 
				$scope.DEFAULT_STORAGE_UNIT, 
				$scope.DIS_LOCATION,
				$scope.MAX_PART_SIZE,
				$scope.MAX_PART_TIME
			];

			$scope.languageSettings = [
				$scope.DEFAULT_LANGUAGE,
				$scope.LOCALIZATION_TODAY,
				$scope.LOCALIZATION_TOMORROW,
				$scope.LOCALIZATION_YESTERDAY
			];

			$scope.dateSettings = [
				$scope.DEFAULT_DATE_INPUT_FORMAT, 
				$scope.DEFAULT_DATE_OUTPUT_FORMAT, 
				$scope.DEFAULT_DATETIME_INPUT_FORMAT, 
				$scope.DEFAULT_DATETIME_OUTPUT_FORMAT, 
				$scope.DEFAULT_DAY_OUTPUT_FORMAT, 
				$scope.DEFAULT_DAYTIME_OUTPUT_FORMAT, 
				$scope.DEFAULT_TIME_OUTPUT_FORMAT, 
				$scope.DEFAULT_YEARTIME_OUTPUT_FORMAT
			];

			$scope.ocrSettings = [
				$scope.OCR_RETRY_COUNT, 
				$scope.OCR_SCHEDULE, 
				$scope.OCR_SERVICE_LOCATION
			];

			$scope.loginSettings = [
				$scope.DELETE_OLD_SESSIONS, 
				$scope.LOGIN_ATTEMPTS, 
				$scope.LOGIN_AUDIT,
				$scope.PW_CHANGE_FREQ,
				$scope.PW_CHANGE_INTERVAL,
				$scope.PW_HOLD_HISTORY,
				$scope.PW_PROFILE,
				$scope.PW_PROFILE_DESC,
				$scope.REMEMBER_COOKIE
			];

			$scope.advancedSettings = [
				$scope.ALLOW_RECURSIVE_SEARCH, 
				$scope.ALLOW_SORT_UNINDEXED, 
				$scope.BUTTON_GRID_WIDTH,
				$scope.CACT_2_TEMPLATE,
				$scope.DOCSDB_DOWN,
				$scope.FTR_SEARCH_QUERY,
				$scope.JOURNALING,
				$scope.KEY_TIMEOUT,
				$scope.LOV_MAX_RESULTS,
				$scope.MAX_LAST_ACTIONS,
				$scope.MAX_UPLOAD_FILESIZE,
				$scope.ORGANIZATION_EDITABLE,
				$scope.SEARCH_MAX_RESULTS,
				$scope.SHOW_WORKLIST_LIMIT,
				$scope.TEMP_DIRECTORY,
				$scope.VIEW_EML,
				$scope.WORKBENCH_DIRECT_FTR
			];
		});

		$scope.updateSettings = function(){
			if(angular.equals(systemsettingsfromserver, $scope.systemsettings)){
				console.log('the same, not saving');
			}else{
				console.log('not the same, saving');
				SystemFactory.save($scope.systemsettings);
			}
		};

		function createVarsFormObjectsInArray(array){
			for(var a = 0; a < array.length; a++){
				$scope[array[a].parameter] = getObjectFromArray(array, 'parameter', array[a].parameter);
			}
		}

		function getObjectFromArray(array, key, value){
			var res = null;
			for (var i = 0; i < array.length; i++) {
				if (array[i][key] === value) {
					res = array[i];
					break;
				}
			}
			return res;
		}
	});
})();