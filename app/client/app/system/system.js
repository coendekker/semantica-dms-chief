'use strict';

angular.module('appSystem', [])
	.config(function ($stateProvider) {
		$stateProvider
			.state('system', {
				url: '/system',
				templateUrl: 'app/system/system.html',
				controller: 'SystemCtrl'
		});
	})

	.factory('SystemModel', function(Restangular){
		var restSystem = Restangular.all('systemsetting');

		Restangular.extendCollection('systemsetting', function(collection){
			collection.create = function(systemData){
				var system = Restangular.restangularizeElement(this.parentResource, systemData, 'systemsetting');
				return system;
			};
			return collection;
		});
		return restSystem;
	})

	.factory('SystemFactory', function($q, SystemModel){
		var factory = {};
		var systems = [];

		factory.getByParameter = function(param){
			var params = _(systems)
				.filter(function(system) { return system.parameter === param; })
				.pluck('value')
				.value();

			if(params && params.length > 0){
				return params[0];
			}
			return null;
		};

		factory.get = function(){
			var deferred = $q.defer();
			SystemModel.getList().then(function(systemsfromserver){
				systems = systemsfromserver;
				deferred.resolve(systems);
			});
			return deferred.promise;
		};

		factory.save = function(system){
			var deferred = $q.defer();
			if(!system.id){
				SystemModel.post(system).then(function(newsystem){
					console.log(newsystem);
					systems.push(newsystem);
					deferred.resolve(newsystem);
			 });

			}else{
				system.save().then(function(savedsystem){
					var index = systems.indexOf(system);
					angular.copy(savedsystem, systems[index]);
					deferred.resolve(savedsystem);
				});

			}
			return deferred.promise;
		};

		factory.remove = function(system){
			system.remove().then(function(){
				var index = systems.indexOf(system);
				systems.splice(index, 1);
			});
		};

		factory.cancel = function(system){
			if(system.id){
				system.get().then(function(cancelsystem){
					var index = systems.indexOf(system);
					angular.copy(cancelsystem, systems[index]);
				});
			}
			
		};
		return factory;
	});
