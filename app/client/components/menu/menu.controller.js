//menu.controller.js
'use strict';

angular.module('appMenu', [])
.controller('MenuCtrl', function ($rootScope, $scope, $log, $location, $translate) {

	$scope.menuItems = [
		{'url':'/users', 'name':'users', 'icon':'fa fa-user'},
		{'url':'/roles','name':'roles', 'icon':'fa fa-credit-card'},
		{'url':'/worklists', 'name':'worklists', 'icon':'fa fa-filter'},
		{'url':'/doctypes', 'name':'doctypes', 'icon':'fa fa-file-text'},
		{'url':'/docprops', 'name':'docprops', 'icon':'fa fa-check-square'},
		{'url':'/lists','name':'lists', 'icon':'fa fa-list'},
		{'url':'/compositions','name':'compositions', 'icon':'fa fa-folder-open'},
		{'url':'/system','name':'system', 'icon':'fa fa-gear'}
		// ,{'url':'/snapmenu', 'name':'snapmenu', 'icon':'fa fa-toggle-left'}
	];

	$scope.selectedMenuItem = null;

	$scope.toggle = function(property){
		// $log.debug(property);
		$scope.selectedMenu = property.name;
		$location.path(property.url);
	};

	$scope.setCurrentMenu = function(url){
		var filteredArray = $scope.menuItems.filter(function (element){
			return element.url === url;
		});
		// console.log(filteredArray);
		if(filteredArray.length > 0){
			$scope.selectedMenu = filteredArray[0].name;
		}
	};

	$scope.checkMenu = function(){
		if($location.path() === '/login'){
			$scope.showMenu = false;
		}else{
			$scope.showMenu = true;
		}
	};
	
	$scope.checkMenu();

	$scope.setCurrentMenu($location.path());

	$rootScope.$on('$stateChangeSuccess', function() {
		console.log('stateChangeSuccess');
		$scope.checkMenu();
		$scope.setCurrentMenu($location.path());
	});
});
