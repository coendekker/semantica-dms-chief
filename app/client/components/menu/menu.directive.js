//menu.directive.js
'use strict';

angular.module('appChief')
  .directive('menuButtons', function () {
    return {
      templateUrl: 'components/menu/menu.html',
      restrict: 'E',
      replace: true,
      controller: 'MenuCtrl'
    };
  });
