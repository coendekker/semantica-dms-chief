'use strict';

angular.module('appChief')
  .directive('navbar', function () {
    return {
      templateUrl: 'components/header/header.html',
      restrict: 'E',
      controller: 'HeaderCtrl'
    };
  });
