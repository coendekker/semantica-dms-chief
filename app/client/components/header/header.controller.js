'use strict';

angular.module('appChief')
.controller('HeaderCtrl', function ($log, $scope, $translate, Auth, Session) {
	$scope.isCollapsed = true;
	$scope.isLoggedIn = Auth.isLoggedIn;
	$scope.isAdmin = Auth.isAdmin;
	$scope.getCurrentUser = Auth.getCurrentUser;
	$scope.greetingA = '';
	$scope.greetingB = '';
	$scope.selectedLanguage = 'en';
	$translate.use('en');

	$scope.chief = 'chief';

	//TODO -> remove row below later
	$scope.greetingA = 'welcome';
	$scope.greetingB = ' '+Session.getName();
	// --

	$scope.$on('AuthChanged', function(){
		$scope.greetingA = 'welcome';
		$scope.greetingB = ' '+Session.getName();
	});

	$scope.changeLanguage = function(language){
		$scope.selectedLanguage = language;
		$translate.use(language);
		$log.debug($scope.selectedLanguage);
	};
});
